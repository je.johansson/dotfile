#!/usr/bin/python3

import sys
import os
import getopt
import configparser
import subprocess
import base64
import inspect

version = 'v1.0'
verbose = False
debug = False
parser = configparser.ConfigParser()
af_ini_file = os.path.expanduser('~') + '/.af_data.ini'


def init():
  global myuser
  global mypasswd
  parser.read(af_ini_file)
  if not parser.has_section('USER'):
    parser.add_section('USER')
  if parser.has_option('USER', 'myuser'):
    myuser = parser['USER']['myuser']
  else:
    myuser = input('User: ')
    parser['USER']['myuser'] = myuser
  if parser.has_option('USER', 'mypasswd'):
    mypasswd = parser['USER']['mypasswd']
  else:
    mypasswd = input('Password: ')
    parser['USER']['mypasswd'] = mypasswd


def usage():
  head, tail = os.path.split(sys.argv[0])
  print(tail, version)
  print()
  print(tail, '[option] <new_user>')
  print('e.g.', tail, '-u jonju -p jsdfjghksdf jooda')
  print('')
  print('Mandatory arguments to long options are mandatory for short options too.')
  print('    -u, --user=USER          user to connect to ldap.')
  print('    -p, --passwd=PASSWD      password to connect to ldap.')
  print('    -v. --verbose            verbose')
  print('    -v. --debug              debug')
  print('    -h, --help               display this help and exit')
  print('')
  print('Content of file:', af_ini_file)
  with open(af_ini_file) as f:
    contents = f.read()
    print(contents)
  f.close()
  os._exit(1)


def debug_log(logline):
          if debug:
                print('* Called from: line', inspect.stack()[1].lineno, inspect.stack()[1].function, '*', logline)


def parse_cmdline():
  global myuser
  global mypasswd
  global debug
  global verbose
  try:
    options, remainder = getopt.getopt(
        sys.argv[1:],
        'v:h:u:p::d',
        [
            'help',
            'user=',
            'passwd=',
            'debug',
        ],
    )
  except getopt.GetoptError as err:
    print(err)
    usage()
  for opt, arg in options:
    if opt in ('-h', '--help'):
      usage()
    elif opt in ('-v', '--verbose'):
      verbose = True
    elif opt in ('-d', '--debug'):
      debug = True
    elif opt in ('-u', '--user'):
      parser['USER']['myuser'] = arg
    elif opt in ('-p', '--passwd'):
      parser['USER']['mypasswd'] = arg
  with open(af_ini_file, 'w') as parserfile:
    parser.write(parserfile)
  if len(remainder) != 1:
    usage()
  if len(remainder[0]) != 5:
    usage()
  return remainder[0]


def x500_fullname(x500_name):
  debug_log(x500_name)
  from re import search
  global myuser
  ldap_host = 'ldap://wp.ams.se'
  ldap_binddn = 'CN=l1' + myuser + 'adm,OU=T1-Accounts,OU=Tier 1,OU=Admin,DC=wp,DC=ams,DC=se'
  ldap_basedn = 'OU=x500users,DC=wp,DC=ams,DC=se'
  stdout = subprocess.run(
      [
          'ldapsearch',
          '-H',
          ldap_host,
          '-D',
          ldap_binddn,
          '-w',
          mypasswd,
          '-x',
          '-b',
          ldap_basedn,
          'cn=' + x500_name,
          'sn',
          'givenname',
      ],
      check=True,
      capture_output=True,
      text=True,
  ).stdout
  lines = stdout.split('\n')
  for line in lines:
    if search('^sn: ', line):
      surName = search('^sn: (.*)', line).group(1)
    if search('^sn:: ', line):
      surName = base64.b64decode(search('^sn:: (.*)', line).group(1)).decode('utf8')
    if search('^givenName: ', line):
      givenName = search('^givenName: (.*)', line).group(1)
    if search('^givenName:: ', line):
      givenName = base64.b64decode(search('^givenName:: (.*)', line).group(1)).decode('utf8')
  return givenName + ' ' + surName


def oc_cmd(cmd):
  debug_log(cmd)
  stdout = subprocess.run(
      cmd,
      check=True,
      capture_output=True,
      text=True,
  ).stdout
  lines = stdout.split('\n')
  for line in lines:
    print(line)


def oc_create_user(l1user, fullname):
  debug_log(l1user + ' ' + fullname)
  oc_cmd(['oc', 'create', 'user', l1user, '--full-name', fullname])
  oc_cmd(['oc', 'create', 'identity', 'Arbetsförmedlingen:CN=' + l1user + ',OU=T1-Accounts,OU=Tier 1,OU=Admin,DC=wp,DC=ams,DC=se'])
  oc_cmd(['oc', 'create', 'useridentitymapping', 'Arbetsförmedlingen:CN=' + l1user + ',OU=T1-Accounts,OU=Tier 1,OU=Admin,DC=wp,DC=ams,DC=se', l1user])


def main():
  init()
  newuser = parse_cmdline()
  debug_log(af_ini_file)
  l1user = 'L1' + newuser + 'adm'
  fullname = x500_fullname(newuser)
  oc_create_user(l1user, fullname)


if __name__ == '__main__':
  main()

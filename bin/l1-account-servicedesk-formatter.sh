#!/bin/bash
#
# Quick and dirty script to create the very specific information that
# servicedesk needs to create l1accounts and bind them to specific
# groups (roles/tasks).
#
# For some reason it's extremely important that the information in the
# ticket looks a certain way, and thus this script will do it's best to
# gather the required information and present it accordingly.
#
# <patrik.martinsson@arbetsformedlingen.se>
#

set -euo pipefail

## $PROG 1.0 - service now formatter [2021-10-10]
## (Credentials are stored in file "~/.af_data")
## 
## Usage: $PROG [OPTION...] [COMMAND]...
## Options:
##   -u, --myuser USER            Set user to use
##   -p, --mypassword PASSWORD    Set password to use
##   -d, --debug                  Debug
## Commands:
##   -h, --help             Displays this help and exists
##   -v, --version          Displays output version and exists
## Examples:
##   $PROG -u l1useradm -p CFYtfhJYTFhgcHJTFY -T    # Login to test and save my credentials
##   $PROG -T                                       # Login to test using my saved credentials

#VARIABLES
PROG=${0##*/}
eval af_data_file="~/.af_data"

#==================================================================
#
#function that writes line of "variabe=Value" into file $af_data_file
#
#V 1.0 
#==================================================================
function set_variable(){
  local variable=$1 #variable to be set
  local value=$2    # value to give to the value
  if ! grep -xq $variable=.* $af_data_file
  then
    echo "myuser=$myuser" >> $af_data_file
  else
    sed -i 's/'${variable}'.*/'${variable}'='${value}'/' $af_data_file
  fi
}

credentials() {
  if [ -s "$af_data_file" ]
  then
    if ! grep -xq myuser=.* $af_data_file
    then
      printf "Enter user name: "
      read myuser
      echo "myuser=$myuser" >> $af_data_file
    fi
    if ! grep -xq mypassword=.* $af_data_file
    then
      printf "Enter password: "
      read mypassword
      echo "mypassword=$mypassword" >> $af_data_file
    fi
  else
    echo "# AF variables used by scripts" > $af_data_file
    printf "Enter user name: "
    read myuser
    echo "myuser=$myuser" >> $af_data_file
    printf "Enter password: "
    read mypassword
    echo "mypassword=$mypassword" >> $af_data_file
  fi
  if [ -s "$af_data_file" ]; then source $af_data_file; fi
}



# * * *
#
# ! EDIT THEESE TWO PARAMETERS !
#

# Name of group that you want your users to be added to.
#
# Note that in Active Directory at AF we call them "Roles" for some
# reason. The group name here should start with "Role-T1-OpenShift-XX"
group_name="Role-T1-OpenShift-ANNONS"


# List of users that you want to add to the specific group mentioned
# above
users=(
   "bwoan"
#  "example-user-1"
#  "example-user-2"
)

#
# ! DO NOT EDIT BELOW THIS LINE !
# * * *




# Variables used in varios places
g_base_user="OU=x500users,DC=wp,DC=ams,DC=se"
g_base_l1="OU=T1-Accounts,OU=Tier 1,OU=Admin,DC=wp,DC=ams,DC=se"
g_ldap_uri="ldap://wp.ams.se"
g_final_out=("")


#
# Functions
#

# Function that will check for roles and tasks according to given
# group_name.
function check_roles_tasks(){

  local base=(
    'OU=T1-Roles,OU=Tier 1,OU=Admin,DC=wp,DC=ams,DC=se'
    'OU=T0-Tasks,OU=Tier 0,OU=Admin,DC=wp,DC=ams,DC=se'
  )

  local cn=(
    "${group_name}"
    "${group_name/Role-T1/Task}"
  )

  i=0
  IFS=$'\n'
  echo " [ INF ] querying ${g_ldap_uri} for role and tasks matching" \
       "'${group_name/Role-T1-/}'"

  # Check roles / tasks
  for base in ${base[@]}; do
    cmd=('ldapsearch'
         '-LLL'
          "-H"
          "${g_ldap_uri}"
          '-D'
          ''"${g_bind_user}"''
          "-w"
          ''"${g_bind_pass}"''
          '-x'
          '-b'
          ''"${base}"''
          'cn='"${cn[i]}"''
          'cn'
    )

    # Do query
    if ! out=$("${cmd[@]}" 2>&1); then
      cmd[7]="*****"
      echo -e "\n [ ERR ] command : '${cmd[@]}' failed"
      echo -e " [ ERR ] output : ${out}\n"
      exit 1
    fi

    # Verify output
    if out=$(echo "${out}" | grep -i "cn: ${cn[i]}"); then
      echo " [ INF ] found '${cn[i]}'"
    else
      echo " [ WRN ] '${cn[i]}' ** NOT ** found, make sure you know"   \
           "what you are doing (have you ordered correct roles/tasks"  \
           "from servicedesk?)"
    fi

    i=$((i+1))
  done
}


# Function that will check if a user is already member of given group,
# no need to create a ticket for that user.
function user_already_member(){

  local base_roles="CN=${group_name},OU=T1-Roles,OU=Tier 1,OU=Admin,DC=wp,DC=ams,DC=se"
  local user="${1}"

  echo " [ INF ] querying ${g_ldap_uri} to see if '${user}' is already a member" \
       "of ${group_name}"

  # Check if given user is a member of given group_name
  cmd=('ldapsearch'
       '-LLL'
        "-H"
        "${g_ldap_uri}"
        '-D'
        ''"${g_bind_user}"''
        "-w"
        ''"${g_bind_pass}"''
        '-x'
        '-b'
        ''"${g_base_l1}"''
        ''"(&(cn=${user})(memberOf:1.2.840.113556.1.4.1941:=${base_roles}))"''
        'cn'
  )

  # Do query
  if ! out=$("${cmd[@]}" 2>&1); then
    cmd[7]="*****"
    echo -e "\n [ ERR ] command : '${cmd[@]}' failed"
    echo -e " [ ERR ] output : ${out}\n"
    exit 1
  fi

  # Verify output
  if out=$(echo "${out}" | grep -i "cn: ${cn[i]}"); then
    echo " [ WRN ] '${user}' is already a member of '${group_name}' - and" \
         "thus alerady have access to openshift, no need to create a"      \
         "service desk ticket for '${user}'"
  else
    echo " [ INF ] '${user}' is ** NOT ** a member of '${group_name}'," \
         "this means you can go a head and create a servicedesk"        \
         " ticket according to the information below"
  fi
}


# Function that will check if a user is already member of given group,
# no need to create a ticket for that user.
user_has_l1_account(){

  local user="${1}"

  echo " [ INF ] querying ${g_ldap_uri} to see if '${user}' exists"

  # Check if given user exists
  cmd=('ldapsearch'
       '-LLL'
        "-H"
        "${g_ldap_uri}"
        '-D'
        ''"${g_bind_user}"''
        "-w"
        ''"${g_bind_pass}"''
        '-x'
        '-b'
        ''"${g_base_l1}"''
        'cn='"${user}"''
        'cn'
  )

  # Do query
  if ! out=$("${cmd[@]}" 2>&1);then
    cmd[7]="*****"
    echo -e "\n [ ERR ] command : '${cmd[@]}' failed"
    echo -e " [ ERR ] output : ${out}\n"
    exit 1
  fi

  # Verify output
  if out=$(echo "${out}" | grep -i "cn: ${user}"); then
    return 0
  else
    return 1
  fi
}


# Function that will read input from the user at start
function read_input() {
  g_bind_pass=$mypassword
  g_bind_user="CN=$myuser,OU=T1-Accounts,OU=Tier 1,OU=Admin,DC=wp,DC=ams,DC=se"
}


# MAIN BEGIN HERE
die() { echo $@ >&2; exit 2; }

credentials

# Read user input
read_input

# Check if given group has role/task cn's
check_roles_tasks

# Loop users, gather info and print accordingly
IFS=$'\n'
for user in ${users[@]}; do

  echo " [ INF ] querying ${g_ldap_uri} for user '${user}'"

  cmd=('ldapsearch'
       '-LLL'
       "-H"
       "${g_ldap_uri}"
       '-D'
       ''"${g_bind_user}"''
       "-w"
       ''"${g_bind_pass}"''
       '-x'
       '-b'
       ''"${g_base_user}"''
       'cn='"${user}"''
       'sn'
       'givenName'
  )

  # Do query
  if ! out=$("${cmd[@]}" 2>&1); then
    cmd[7]="*****"
    echo -e "\n [ ERR ] command : '${cmd[@]}' failed"
    echo -e " [ ERR ] output : ${out}\n"
    exit 1
  fi

  # No user, bail out
  if ! [[ "${out}" ]];then
    echo -e "\n [ ERR ] could not find user '${user}' \n"
    exit 1
  fi

  # Perl is used to get correct character encoding as well as reverse the
  # line
  str=$(echo "${out}" |
        perl -MMIME::Base64                                                               \
             -MEncode=decode                                                              \
             -n                                                                           \
             -00                                                                          \
             -e 's/\n +//g;s/(?<=:: )(\S+)/decode("LATIN-1",decode_base64($1))/eg;print'  |
             grep -e sn:                                                                  \
                  -e givenName:                                                           |
             sed  -E -e 's/sn:(:*) //'                                                    \
                     -e 's/givenName:(:*) //'                                             |
             perl -e "print reverse(<>)"                                                  |
             tr '\n' ',')

  if ! [[ ${str} ]];then
    echo -e "\n [ ERR ] : could not convert string '${str}' properly \n"
    exit 1
  fi

  echo " [ INF ] found '${user}' with full name '${str%,}'"

  if user_has_l1_account "l1${user}adm"; then
    echo " [ INF ] '${user}' has an L1 account"
    user_already_member "l1${user}adm"
    str_idiot=" - Konto finns"
  else
    echo " [ INF ] '${user}' does ** NOT ** have an L1 account"
    str_idiot=" - Nytt konto"
  fi

  g_final_out+=("${user},${str}1,${group_name}${str_idiot}")
done

# Print output
echo " [ INF ] verify info below and copy it to your service desk ticket"
echo ""
echo "Avser access till Openshift"
for i in ${g_final_out[@]}; do
  echo "$i"
done
echo ""

#!/bin/bash


login=$1
fullname=$2

oc create user $login --full-name "${fullname}"
oc create identity "Arbetsförmedlingen:CN=$login,OU=T1-Accounts,OU=Tier 1,OU=Admin,DC=wp,DC=ams,DC=se"
oc create useridentitymapping "Arbetsförmedlingen:CN=$login,OU=T1-Accounts,OU=Tier 1,OU=Admin,DC=wp,DC=ams,DC=se" $login

oc get users | grep $login
oc get identity | grep $login

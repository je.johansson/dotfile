#!/bin/bash

set -euo pipefail

## $PROG V3.3 - oc login to node [2022-08-13]
## (Credentials are stored in file "~/.af_data")
##  
## Usage: $PROG [OPTION...] [COMMAND]...
## Options:
##   -u, --myuser USER            Set user to use
##   -p, --mypassword PASSWORD    Set password to use
##   -d, --debug                  Debug
##
## Commands:
##   -3,  --tocp             Login to openshift 3 test
##   -4,  --tocp4            Login to TOCP4
##   -4P, --pocp4            Login to POCP4
##   -4p, --pocp4            Login to POCP4
##   -A,  --proda            Login to openshift 3 prod A
##   -a,  --proda            Login to openshift 3 prod A
##   -B,  --prodb            Login to openshift 3 prod B
##   -b,  --prodb            Login to openshift 3 prod B
##   -s,  --server           Login to server
##   -t,  --token            Login with token
##   -h,  --help             Displays this help and exists
##   -v,  --version          Displays output version and exits
##
## Examples:
##   $PROG -u l1useradm -p CFYtfhJYTFhgcHJTFY -3    # Login to tocp3 and save my credentials
##   $PROG -4                                       # Login to tocp4 using my saved credentials
##  
##   Login to ocp4 and save my credentials:
##   $PROG --token=sha256~xJkpgFOmqun1ET55hjklz3y4RQsHuC40r29c4vj8RE --server=https://api.tocp4.arbetsformedlingen.se:6443
## 

#VARIABLES
PROG=${0##*/}
OWNPATH=${0}
eval af_data_file="~/.af_data"

#==================================================================
#
#function that writes line of "variabe=Value" into file $af_data_file
#
#V 1.0
#==================================================================
set_variable(){
  local variable=$1 #variable to be set
  local value=$2    # value to give to the value
  if ! grep -xq $variable=.* $af_data_file
  then
    echo "$variable=$value" >> $af_data_file
    echo "New Variable: $variable=$value" 
  else
    sed -i 's/'${variable}'.*/'${variable}'='${value}'/' $af_data_file 2> /dev/null
    echo "Updated Variable: $variable=$value"
  fi
}


printargs(){
  i=1;
  for temp in "$@"
  do
    echo "Var - $i: $temp";
    i=$((i + 1));
  done
}


credentials() {
  if [ -s "$af_data_file" ]
  then
    if ! grep -xq myuser=.* $af_data_file
    then
      printf "Enter user name: "
      read myuser
      echo "myuser=$myuser" >> $af_data_file
    fi
    if ! grep -xq mypassword=.* $af_data_file
    then
      printf "Enter password: "
      read mypassword
      echo "mypassword=$mypassword" >> $af_data_file
    fi
  else
    echo "# AF variables used by scripts" > $af_data_file
    printf "Enter user name: "
    read myuser
    echo "myuser=$myuser" >> $af_data_file
    printf "Enter password: "
    read mypassword
    echo "mypassword=$mypassword" >> $af_data_file
  fi
  if [ -s "$af_data_file" ]; then source $af_data_file; fi
}

# Normal OSX runs with BSD grep which does not support perl regexp syntax which we use to extract access token from curl request
available_grep() {
  kernel=$(uname -s)
  available_grep=$([[ $kernel == "Darwin" ]] && printf "ggrep" || printf "grep")
  $available_grep --help > /dev/null || (printf "$available_grep not available, please install.\n" && exit 1)
  printf $available_grep
}

login_request() {
  local url=$1
  local request_url="/oauth/authorize?client_id=openshift-challenging-client&response_type=token"
  grep_exec=$(available_grep)
  curl_response=$(curl -u "$myuser:$mypassword" $1$request_url -skv -H "X-CSRF-Token: xxx" --stderr - | $grep_exec -oP "access_token=\K[^&]*")
  [[ -z $curl_response ]] && (printf "Failed to get token from URL, please check that your credentials are valid\n" && exit 1)
  printf $curl_response
}

login_oc() {
  local url=$1
  local token=$2
  [[ -z $url ]] && printf "Please supply url\n"
  [[ -z $token ]] && printf "Please supply token\n"
  oc login $url --token=$token
}

myuser() {
  myuser=$1
  set_variable myuser $myuser
  return 1
}

mypassword() {
  mypassword=$1
  set_variable mypassword $mypassword
  return 1
}

server() {
  if echo $1 | grep -q "https://api.tocp4.arbetsformedlingen.se:6443"
  then
    tocp4
  fi
  if echo $1 | grep -q "https://api.pocp4.arbetsformedlingen.se:6443"
  then
    pocp4
  fi
  return 1
}

token() {
  if echo $@ | grep -q "https://api.tocp4.arbetsformedlingen.se:6443"
  then
    mytocp4=$1
    set_variable mytocp4 $mytocp4
  fi
  if echo $@ | grep -q "https://api.pocp4.arbetsformedlingen.se:6443"
  then
    mypocp4=$1
    set_variable mypocp4 $mypocp4
  fi
  return 1
}

debug() {
  set -euxo pipefail
}

tocp() {
  local test_cluster=https://tocpconsole.arbetsformedlingen.se:443
  local token=$(login_request $test_cluster)
  login_oc $test_cluster $token
}

tocp4() {
  oc login --token=$mytocp4 --server=https://api.tocp4.arbetsformedlingen.se:6443
}

pocp4() {
  oc login --token=$mypocp4 --server=https://api.pocp4.arbetsformedlingen.se:6443
}

proda() {
  local prod_cluster_a=https://ocpcluster-a.arbetsformedlingen.se:443
  local token=$(login_request $prod_cluster_a)
  login_oc $prod_cluster_a $token
}

prodb() {
  local prod_cluster_b=https://pocpconsole.arbetsformedlingen.se:443
  local token=$(login_request $prod_cluster_b)
  login_oc $prod_cluster_b $token
}

help() {
  grep "^##" "$0" | sed -e "s/^...//" -e "s/\$PROG/$PROG/g"
  cat $af_data_file
  exit 0
}

version() {
  help | head -1
  exit 0
}


# MAIN BEGIN HERE
die() { echo $@ >&2; exit 2; }

credentials

# Remove = from commandline
set -- $(echo $@ | sed -e 's/=/ /g')

[ $# = 0 ] && help
while [ $# -gt 0 ]; do
  CMD=$(grep -m 1 -Po "^## *$1, *--\K[^= ]*|^##.* --\K${1#--}(?:[= ])" $OWNPATH | sed -e "s/-/_/g")
#  if [ -z "$CMD" ]; then echo "ERROR: Command '$1' not supported"; exit 1; fi
  if type $CMD 2>/dev/null | grep -q 'function'
  then
    shift; eval "$CMD" $@ || shift $? 2> /dev/null
  else
    echo "Eroor $CMD"
    exit 1
  fi


done


#!/bin/bash

target=$1

count=$( ping -i 0.2 -c 1 $target | grep icmp* | wc -l )

if [ $count -eq 0 ]

then
  printf "%s " $1 >> /root/pingfail.txt
  date >> /root/pingfail.txt

fi
